<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup">
					<h1 class="hgroup-title">Financing</h1>
				</div><!-- .hgroup -->
				
				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br /> Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus.
				</p><!-- .excerpt -->
				
				<div class="main-body">				
					<div class="content">

						<div class="article-body center">
						
							<p>
								Cras ut pellentesque magna, eu rhoncus dolor. Integer vulputate tortor vel nisi euismod, eget bibendum turpis tempus. Nulla dictum
								enim a sem cursus fermentum. Praesent diam arcu, mollis quis rutrum et, rhoncus sit amet dui. Interdum et malesuada fames ac ante
								ipsum primis in faucibus. Aenean est est, vulputate sed mauris eget, vulputate gravida eros. Nulla in diam ornare justo vehicula feugiat
								quis vel purus. Cras interdum erat sit amet nisl ultrices, ut rutrum turpis imperdiet. Nullam eget tellus a justo ornare ornare. Lorem
								ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tincidunt odio quis ligula auctor, eget laoreet nisi tempus. Nulla et purus
								sit amet nibh maximus venenatis. Pellentesque sed quam felis. Proin sed semper risus. Morbi sit amet nunc at dui pulvinar auctor.
							</p>
							
							<a href="#" class="button fill">Request</a>
						
						</div><!-- .article-body -->
						
					</div><!-- .content -->
				</div><!-- .main-body -->
			
			</article>
			
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>