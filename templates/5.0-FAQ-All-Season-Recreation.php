<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="hgroup-title">Frequently Asked Questions</h1>
			</div><!-- .hgroup -->
			
			<p class="excerpt">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br /> Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus.
			</p><!-- .excerpt -->

			<div class="faq-grid grid eqh fill">
			
				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item faq-item">
						
						<div class="faq-head lighter-secondary-bg">
							Why has my ad been suspended?
						</div><!-- .faq-head -->
						
						<div class="faq-body">
							<div class="p-wrap">
								<p>
									Ads are normally suspended if they are considered over posting, 
									have inappropriate content or not accepted under the free listings.
									Please refer to the other questions for a definition of over posting and 
									what is not accepted under the free listings. NLClassifieds also
									reserves the right to reject any classified non convallis nisi euismod nec. Aenean ornare 
									eleifend purus, sit amet finibus velit hendrerit vel. Vivamus in orci at enim porta 
									tincidunt quis sed risus. Quisque consequat scelerisque eros, quis rhoncus diam 
									posuere id. Morbi eu tempus odio.
								</p>
							</div><!-- .p-wrap -->
							<span class="expand t-fa-abs">More</span>
						</div><!-- .faq-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item faq-item">
						
						<div class="faq-head lighter-secondary-bg">
							What is considered over posting?
						</div><!-- .faq-head -->
						
						<div class="faq-body">
							<div class="p-wrap">
								<p>
									Ads are normally suspended if they are considered over posting, 
									have inappropriate content or not accepted under the free listings.
									Please refer to the other questions for a definition of over posting and 
									what is not accepted under the free listings. NLClassifieds also
									reserves the right to reject any classified non convallis nisi euismod nec. Aenean ornare 
									eleifend purus, sit amet finibus velit hendrerit vel. Vivamus in orci at enim porta 
									tincidunt quis sed risus. Quisque consequat scelerisque eros, quis rhoncus diam 
									posuere id. Morbi eu tempus odio.
								</p>
							</div><!-- .p-wrap -->
							<span class="expand t-fa-abs">More</span>
						</div><!-- .faq-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item faq-item">
						
						<div class="faq-head lighter-secondary-bg">
							How often are the ads reviewed and made public?
						</div><!-- .faq-head -->
						
						<div class="faq-body">
							<div class="p-wrap">
								<p>
									Ads are normally suspended if they are considered over posting, 
									have inappropriate content or not accepted under the free listings.
									Please refer to the other questions for a definition of over posting and 
									what is not accepted under the free listings. NLClassifieds also
									reserves the right to reject any classified non convallis nisi euismod nec. Aenean ornare 
									eleifend purus, sit amet finibus velit hendrerit vel. Vivamus in orci at enim porta 
									tincidunt quis sed risus. Quisque consequat scelerisque eros, quis rhoncus diam 
									posuere id. Morbi eu tempus odio.
								</p>
							</div><!-- .p-wrap -->
							<span class="expand t-fa-abs">More</span>
						</div><!-- .faq-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item faq-item">
						
						<div class="faq-head lighter-secondary-bg">
							Why are ads reviewed before they can be seen on the site? 
						</div><!-- .faq-head -->
						
						<div class="faq-body">
							<div class="p-wrap">
								<p>
									Ads are normally suspended if they are considered over posting, 
									have inappropriate content or not accepted under the free listings.
									Please refer to the other questions for a definition of over posting and 
									what is not accepted under the free listings. NLClassifieds also
									reserves the right to reject any classified non convallis nisi euismod nec. Aenean ornare 
									eleifend purus, sit amet finibus velit hendrerit vel. Vivamus in orci at enim porta 
									tincidunt quis sed risus. Quisque consequat scelerisque eros, quis rhoncus diam 
									posuere id. Morbi eu tempus odio.
								</p>
							</div><!-- .p-wrap -->
							<span class="expand t-fa-abs">More</span>
						</div><!-- .faq-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->

			
				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item faq-item">
						
						<div class="faq-head lighter-secondary-bg">
							Why has my ad been suspended?
						</div><!-- .faq-head -->
						
						<div class="faq-body">
							<div class="p-wrap">
								<p>
									Ads are normally suspended if they are considered over posting, 
									have inappropriate content or not accepted under the free listings.
									Please refer to the other questions for a definition of over posting and 
									what is not accepted under the free listings. NLClassifieds also
									reserves the right to reject any classified non convallis nisi euismod nec. Aenean ornare 
									eleifend purus, sit amet finibus velit hendrerit vel. Vivamus in orci at enim porta 
									tincidunt quis sed risus. Quisque consequat scelerisque eros, quis rhoncus diam 
									posuere id. Morbi eu tempus odio.
								</p>
							</div><!-- .p-wrap -->
							<span class="expand t-fa-abs">More</span>
						</div><!-- .faq-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item faq-item">
						
						<div class="faq-head lighter-secondary-bg">
							What is considered over posting?
						</div><!-- .faq-head -->
						
						<div class="faq-body">
							<div class="p-wrap">
								<p>
									Ads are normally suspended if they are considered over posting, 
									have inappropriate content or not accepted under the free listings.
									Please refer to the other questions for a definition of over posting and 
									what is not accepted under the free listings. NLClassifieds also
									reserves the right to reject any classified non convallis nisi euismod nec. Aenean ornare 
									eleifend purus, sit amet finibus velit hendrerit vel. Vivamus in orci at enim porta 
									tincidunt quis sed risus. Quisque consequat scelerisque eros, quis rhoncus diam 
									posuere id. Morbi eu tempus odio.
								</p>
							</div><!-- .p-wrap -->
							<span class="expand t-fa-abs">More</span>
						</div><!-- .faq-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item faq-item">
						
						<div class="faq-head lighter-secondary-bg">
							How often are the ads reviewed and made public?
						</div><!-- .faq-head -->
						
						<div class="faq-body">
							<div class="p-wrap">
								<p>
									Ads are normally suspended if they are considered over posting, 
									have inappropriate content or not accepted under the free listings.
									Please refer to the other questions for a definition of over posting and 
									what is not accepted under the free listings. NLClassifieds also
									reserves the right to reject any classified non convallis nisi euismod nec. Aenean ornare 
									eleifend purus, sit amet finibus velit hendrerit vel. Vivamus in orci at enim porta 
									tincidunt quis sed risus. Quisque consequat scelerisque eros, quis rhoncus diam 
									posuere id. Morbi eu tempus odio.
								</p>
							</div><!-- .p-wrap -->
							<span class="expand t-fa-abs">More</span>
						</div><!-- .faq-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item faq-item">
						
						<div class="faq-head lighter-secondary-bg">
							Why are ads reviewed before they can be seen on the site? 
						</div><!-- .faq-head -->
						
						<div class="faq-body">
							<div class="p-wrap">
								<p>
									Ads are normally suspended if they are considered over posting, 
									have inappropriate content or not accepted under the free listings.
									Please refer to the other questions for a definition of over posting and 
									what is not accepted under the free listings. NLClassifieds also
									reserves the right to reject any classified non convallis nisi euismod nec. Aenean ornare 
									eleifend purus, sit amet finibus velit hendrerit vel. Vivamus in orci at enim porta 
									tincidunt quis sed risus. Quisque consequat scelerisque eros, quis rhoncus diam 
									posuere id. Morbi eu tempus odio.
								</p>
							</div><!-- .p-wrap -->
							<span class="expand t-fa-abs">More</span>
						</div><!-- .faq-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .faq-grid -->
			
			<div class="center">
				<a href="#" class="button fill">See More</a>
			</div><!-- .center -->
			
		</div><!-- .sw -->
	</section>

	<section class="d-bg lighter-secondary-bg">
		<div class="sw">
			<?php include('inc/i-testimonial.php'); ?>
		</div>
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>