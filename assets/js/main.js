var ns = 'JAC-DEALFINDER';
window[ns] = {};

// Utilities used pretty much everywhere 
// @codekit-append "scripts/debounce.js"
// @codekit-append "scripts/tests.js"
// @codekit-append "scripts/image.loader.js"

// Map
// @codekit-append "scripts/map/html.marker.js"
// @codekit-append "scripts/gmap.js";

// @codekit-append "scripts/anchors.external.popup.js"
// @codekit-append "scripts/standard.accordion.js"
// @codekit-append "scripts/magnific.popup.js"
// @codekit-append "scripts/custom.select.js"
// @codekit-append "scripts/lazy.images.js"
// @codekit-append "scripts/nav.js"
// @codekit-append "scripts/swiper.js"
// @codekit-append "scripts/blocks.js"
// @codekit-append "scripts/preventOverScroll.js"

// DealFinder specific scripts
// @codekit-append "scripts/contact.map.js"
// @codekit-append "scripts/faqs.js"

// @codekit-append "global.js"